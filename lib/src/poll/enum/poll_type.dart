enum PollType {
  // Voters should see results as soon as they have voted.
  disclosedUnstable,
  disclosed,
  //Results should be only revealed when the poll is ended.
  undisclosedUnstable,
  undisclosed
}

String getEnumValueString(PollType pollType) {
  switch (pollType) {
    case PollType.disclosedUnstable:
      return 'org.matrix.msc3381.poll.disclosed';
    case PollType.disclosed:
      return 'm.poll.disclosed';
    case PollType.undisclosedUnstable:
      return 'org.matrix.msc3381.poll.undisclosed';
    default:
      return 'm.poll.undisclosed';
  }
}

