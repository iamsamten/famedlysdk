import 'package:matrix/matrix.dart';

class PollQuestion{
  final String question;

  PollQuestion({required this.question});

  Map<String, dynamic> toJson() {
    return {
      MessageTypes.Text: question,
    };
  }
}