import 'package:matrix/src/poll/models/poll_answer.dart';
import 'package:matrix/src/poll/models/poll_question.dart';

class PollCreateContent{
  final PollQuestion question;
  final String pollType;
  final int maxSelection = 1;
  final List<PollAnswer> answers;

  PollCreateContent({
    required this.question,
    required this.pollType,
    required this.answers
  });

  Map<String, dynamic> toJson() {
    return {
      'question': question.toJson(),
      'kind': pollType,
      'max_selections': maxSelection,
      'answers': answers
    };
  }

}