import 'package:matrix/matrix.dart';
class PollMessage {
  final String mimetype;
  final String body;
  PollMessage({required this.mimetype, required this.body});

  Map<String, dynamic> toJson() {
    return {
      'mimetype': mimetype,
      'body': body
    };
  }
}