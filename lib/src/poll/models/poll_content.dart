import 'package:matrix/src/poll/models/poll_create_content.dart';
import 'package:matrix/src/poll/models/poll_message_content.dart';

class PollContent {
  final PollCreateContent pollCreateContent;
  final PollMessageContent pollMessageContent;

  PollContent({
    required this.pollCreateContent,
    required this.pollMessageContent
  });

  Map<String, dynamic> toJson() {
    return {
      'm.poll.start': pollCreateContent.toJson(),
      'm.message': pollMessageContent.toJson()
    };
  }
}