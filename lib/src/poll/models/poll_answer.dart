class PollAnswer {
  final String optionId;
  final String answer;

  PollAnswer({required this.optionId,
        required this.answer,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': optionId,
      'm.text': answer
    };
  }
}