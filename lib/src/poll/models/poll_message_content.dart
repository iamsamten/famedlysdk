import 'package:matrix/matrix.dart';
import 'package:matrix/src/poll/models/poll_message.dart';
class PollMessageContent{
  final List<PollMessage> pollMessages;
  PollMessageContent({required this.pollMessages});

  Map<String, dynamic> toJson() {
    return {
      'm.message': pollMessages
    };
  }
}